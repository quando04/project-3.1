﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project3Unity.Controllers
{
    [Authorize(Roles="Administrator,User")]
    public class SuggestionController : Controller
    {
        private project3Entities db = new project3Entities();

        //
        // GET: /Suggestion/
        [AllowAnonymous]
        public ActionResult Index(int? page, string searchString)
        {
            int pageNumber = page ?? 1;
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);

            if (User.Identity.IsAuthenticated)
            {
                List<string> userRoles = new List<string>();
                var airs = db.AccountsInRoles.Where(m => m.accEmail.Equals(User.Identity.Name)).ToList();
                foreach (AccountsInRole item in airs)
                {
                    userRoles.Add(item.Role.roleName);
                }
                ViewBag.UserRoles = userRoles;
            }

            var suggestions = (from s in db.Suggestions
                               orderby s.sgtID descending
                               select s).Include(s => s.Account);

            // Filter the data through the search string
            if (!String.IsNullOrWhiteSpace(searchString))
            {
                suggestions = suggestions.Where(s =>
                    s.Account.accUsername.ToUpper().Contains(searchString.ToUpper()) ||
                    s.sgtTitle.ToUpper().Contains(searchString.ToUpper()));
            }

            return View(suggestions.ToPagedList(pageNumber, 10));
        }

        //
        // GET: /Suggestion/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Suggestion suggestion = db.Suggestions.Find(id);
            if (suggestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            return View(suggestion);
        }

        //
        // GET: /Suggestion/Create

        public ActionResult Create()
        {
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);

            return View();
        }

        //
        // POST: /Suggestion/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Suggestion suggestion)
        {
            if (ModelState.IsValid)
            {
                db.Suggestions.Add(suggestion);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            ModelState.AddModelError("", "Invalid title or suggestion");
            return View(suggestion);
        }

        //
        // GET: /Suggestion/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id = 0)
        {
            Suggestion suggestion = db.Suggestions.Find(id);
            if (suggestion == null)
            {
                return HttpNotFound();
            }
            //ViewBag.accEmail = new SelectList(db.Accounts, "accEmail", "accUsername", suggestion.accEmail);
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            return View(suggestion);
        }

        //
        // POST: /Suggestion/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(Suggestion suggestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(suggestion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.accEmail = new SelectList(db.Accounts, "accEmail", "accUsername", suggestion.accEmail);
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            return View(suggestion);
        }

        //
        // GET: /Suggestion/Delete/5
        [Authorize(Roles="Administrator")]
        public ActionResult Delete(int id = 0)
        {
            Suggestion suggestion = db.Suggestions.Find(id);
            if (suggestion == null)
            {
                return HttpNotFound();
            }
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            return View(suggestion);
        }

        //
        // POST: /Suggestion/Delete/5
        [Authorize(Roles = "Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Suggestion suggestion = db.Suggestions.Find(id);
            db.Suggestions.Remove(suggestion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}