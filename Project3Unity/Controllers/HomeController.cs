﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project3Unity.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "About";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact Us";

            return View();
        }

        [Authorize(Roles="Administrator,User")]
        public ActionResult Game()
        {
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);

            return View();
        }

        [HttpPost, ActionName("Game")]
        [Authorize(Roles = "Administrator,User")]
        public ActionResult DownloadGame()
        {
            string filename = "/Game/SNAKEY.rar";
            if (filename != "")
            {
                string path = Server.MapPath(filename);
                FileInfo file = new FileInfo(path);
                if (file.Exists)
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    Response.AddHeader("Content-Length", file.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    Response.WriteFile(file.FullName);
                    Response.End();
                }
                else
                {
                    Response.Write("This file does not exist.");
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult Help()
        {
            return View();
        }
    }
}
