﻿using PagedList;
using Project3Unity.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Project3Unity.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class HighScoreController : Controller
    {
        private project3Entities db = new project3Entities();

        //
        // GET: /HighScore/
        [AllowAnonymous]
        public ActionResult Index(int? page, string searchString)
        {
            List<ScoreRank> rankedScores = new List<ScoreRank>();
            int pageNumber = page ?? 1;

            var highScores = (from s in db.HighScores
                              orderby s.srcHS descending
                              select s).Include(h => h.Account);

            // Rank the scores
            int rank = 1;
            foreach (HighScore item in highScores)
            {
                rankedScores.Add(new ScoreRank() { Rank = rank, Score = item });
                rank++;
            }

            // Filter the data through the search string
            if (!String.IsNullOrWhiteSpace(searchString))
            {
                IEnumerable<ScoreRank> filteredRankedScores = new List<ScoreRank>();
                int searchNumber;
                if (int.TryParse(searchString, out searchNumber))
                {
                    filteredRankedScores = rankedScores.Where(rs =>
                        rs.Rank == searchNumber ||
                        rs.Score.Account.accUsername.ToUpper().Contains(searchString.ToUpper()));
                }
                else
                {
                    filteredRankedScores = rankedScores.Where(rs =>
                        rs.Score.Account.accUsername.ToUpper().Contains(searchString.ToUpper()));
                }
                return View(filteredRankedScores.ToPagedList(pageNumber, 10));
            }
            
            return View(rankedScores.ToPagedList(pageNumber, 10));
        }

        //
        // GET: /HighScore/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /HighScore/Create

        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Create(HighScore highscore)
        {
            if (ModelState.IsValid)
            {
                db.HighScores.Add(highscore);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(highscore);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}