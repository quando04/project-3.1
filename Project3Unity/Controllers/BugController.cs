﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project3Unity.Controllers
{
    [Authorize(Roles = "Administrator,User")]
    public class BugController : Controller
    {
        private project3Entities db = new project3Entities();

        //
        // GET: /Bug/
        [AllowAnonymous]
        public ActionResult Index(int? page, string searchString)
        {
            int pageNumber = page ?? 1;
            ViewBag.Leaderboard = Utilities.GetTopScores(10);

            // If user is authenticated, pass to ViewBag all roleNames associated with the user
            // The list is used to deny normal users from editing and deleting bug reports
            if (User.Identity.IsAuthenticated)
            {
                List<string> userRoles = new List<string>();
                var airs = db.AccountsInRoles.Where(m => m.accEmail.Equals(User.Identity.Name)).ToList();
                foreach (AccountsInRole item in airs)
                {
                    userRoles.Add(item.Role.roleName);
                }
                ViewBag.UserRoles = userRoles;
            }

            var bugs = (from b in db.Bugs
                        orderby b.bugID descending
                        select b).Include(b => b.Account);

            // Filter the data through the search string
            if (!String.IsNullOrWhiteSpace(searchString))
            {
                bugs = bugs.Where(b =>
                    b.Account.accUsername.ToUpper().Contains(searchString.ToUpper()) ||
                    b.bugTitle.ToUpper().Contains(searchString.ToUpper()));
            }
            
            return View(bugs.ToPagedList(pageNumber, 10));
        }

        //
        // GET: /Bug/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Bug bug = db.Bugs.Find(id);
            if (bug == null)
            {
                return HttpNotFound();
            }

            // Preview file
            if (bug.bugFile != null)
            {
                string directory = HttpRuntime.AppDomainAppPath + "Upload/Bug";
                //string testurl = Path.Combine(new string[] { HttpRuntime.AppDomainAppPath, "Upload", "Bug", bug.bugFile });
                string fileUrl = Path.Combine(directory, bug.bugFile);
                FileInfo file = new FileInfo(fileUrl);
                string ext = file.Extension;

                //Determine if the file associated is an image or a video
                bool isImage = false, isVideo = false;
                string[] imageExts = { ".jpg", ".jpeg", ".png", ".gif" };
                foreach (string item in imageExts)
                {
                    if (ext.Equals(item))
                    {
                        isImage = true;
                        break;
                    }
                }
                if (!isImage)
                {
                    if (ext.Equals(".mp4"))
                    {
                        isVideo = true;
                    }
                }

                ViewBag.IsImage = isImage;
                ViewBag.IsVideo = isVideo;
                ViewBag.FileExtension = ext;
                ViewBag.FileUrl = "/Upload/Bug/" + bug.bugFile;
            }

            ViewBag.Leaderboard = Utilities.GetTopScores(10);
            return View(bug);
        }

        //
        // GET: /Bug/Create
        public ActionResult Create()
        {
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);

            return View();
        }

        //
        // POST: /Bug/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Bug bug, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                // Add file to the filesystem
                string bugFile = null;
                if (file != null)
                {
                    if (file.ContentLength > 262144000)
                    {
                        ModelState.AddModelError("", "File size is too large, must be at most 250MB");
                        return View(bug);
                    }
                    string fileExt = Path.GetExtension(file.FileName);
                    Guid newFileName = Guid.NewGuid();
                    bugFile = newFileName + fileExt;

                    // Upload the file to the base directory
                    string directory = HttpRuntime.AppDomainAppPath + "Upload/Bug";
                    //string directory = AppDomain.CurrentDomain.BaseDirectory + "Upload/Bug";
                    file.SaveAs(Path.Combine(directory, bugFile));
                }

                bug.bugFile = bugFile;
                db.Bugs.Add(bug);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            ModelState.AddModelError("", "Invalid form submission");
            return View(bug);
        }

        //
        // GET: /Bug/Edit/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id = 0)
        {
            Bug bug = db.Bugs.Find(id);
            if (bug == null)
            {
                return HttpNotFound();
            }

            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            return View(bug);
        }

        //
        // POST: /Bug/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(string oldFile, Bug bug, HttpPostedFileBase newFile)
        {
            if (ModelState.IsValid)
            {
                // Add file to the filesystem and delete old file
                string bugFile = null;
                if (newFile != null)
                {
                    if (newFile.ContentLength > 262144000)
                    {
                        ModelState.AddModelError("", "File size is too large, must be at most 250MB");
                        ViewBag.LeaderBoard = Utilities.GetTopScores(10);
                        return View(bug);
                    }
                    string fileExt = Path.GetExtension(newFile.FileName);
                    Guid newFileName = Guid.NewGuid();
                    bugFile = newFileName + fileExt;

                    // Upload the file to the base directory
                    string directory = HttpRuntime.AppDomainAppPath + "Upload/Bug";
                    newFile.SaveAs(Path.Combine(directory, bugFile));

                    // Delete old file
                    if (!oldFile.Equals(""))
                    {
                        string oldPath = Server.MapPath("~/Upload/Bug/" + oldFile);
                        FileInfo old = new FileInfo(oldPath);
                        if (old.Exists)
                        {
                            old.Delete();
                        }
                    }
                }

                bug.bugFile = bugFile;
                db.Bugs.Attach(bug);
                var entry = db.Entry(bug);
                entry.Property(e => e.bugTitle).IsModified = true;
                entry.Property(e => e.bugDesc).IsModified = true;
                entry.Property(e => e.bugFile).IsModified = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            ModelState.AddModelError("", "Invalid form submission");
            return View(bug);
        }

        //
        // GET: /Bug/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id = 0)
        {
            Bug bug = db.Bugs.Find(id);
            if (bug == null)
            {
                return HttpNotFound();
            }

            // Preview file
            if (bug.bugFile != null)
            {
                string directory = HttpRuntime.AppDomainAppPath + "Upload/Bug";
                string fileUrl = Path.Combine(directory, bug.bugFile);
                FileInfo file = new FileInfo(fileUrl);
                string ext = file.Extension;

                //Determine if the file associated is an image or a video
                bool isImage = false, isVideo = false;
                string[] imageExts = { ".jpg", ".jpeg", ".png", ".gif" };
                foreach (string item in imageExts)
                {
                    if (ext.Equals(item))
                    {
                        isImage = true;
                        break;
                    }
                }
                if (!isImage)
                {
                    if (ext.Equals(".mp4"))
                    {
                        isVideo = true;
                    }
                }

                ViewBag.IsImage = isImage;
                ViewBag.IsVideo = isVideo;
                ViewBag.FileExtension = ext;
                ViewBag.FileUrl = "/Upload/Bug/" + bug.bugFile;
            }

            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            return View(bug);
        }

        //
        // POST: /Bug/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public ActionResult DeleteConfirmed(int id)
        {
            Bug bug = db.Bugs.Find(id);
            db.Bugs.Remove(bug);
            db.SaveChanges();

            // Delete file associated with the deleted bug report
            if (bug.bugFile != null)
            {
                string path = Server.MapPath("~/Upload/Bug/" + bug.bugFile);
                FileInfo file = new FileInfo(path);
                if (file.Exists)
                {
                    file.Delete();
                }
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}