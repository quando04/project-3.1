﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project3Unity.Controllers
{
    [Authorize(Roles = "Administrator,User")]
    public class ReviewController : Controller
    {
        private project3Entities db = new project3Entities();

        //
        // GET: /Review/
        [AllowAnonymous]
        public ActionResult Index(int? page, string searchString)
        {
            int pageNumber = page ?? 1;
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);

            if (User.Identity.IsAuthenticated)
            {
                List<string> userRoles = new List<string>();
                var airs = db.AccountsInRoles.Where(m => m.accEmail.Equals(User.Identity.Name)).ToList();
                foreach (AccountsInRole item in airs)
                {
                    userRoles.Add(item.Role.roleName);
                }
                ViewBag.UserRoles = userRoles;
            }

            var reviews = (from r in db.Reviews
                           orderby r.revID descending
                           select r).Include(r => r.Account);

            // Filter the data through the search string
            if (!String.IsNullOrWhiteSpace(searchString))
            {
                int searchNumber;
                if (int.TryParse(searchString, out searchNumber))
                {
                    reviews = reviews.Where(r =>
                        r.revRating == searchNumber ||
                        r.Account.accUsername.ToUpper().Contains(searchString.ToUpper()) ||
                        r.revTitle.ToUpper().Contains(searchString.ToUpper()));
                }
                else
                {
                    reviews = reviews.Where(r =>
                        r.Account.accUsername.ToUpper().Contains(searchString.ToUpper()) ||
                        r.revTitle.ToUpper().Contains(searchString.ToUpper()));
                }
            }

            return View(reviews.ToPagedList(pageNumber, 10));
        }

        //
        // GET: /Review/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            return View(review);
        }

        //
        // GET: /Review/Create
        public ActionResult Create()
        {
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);

            return View();
        }

        //
        // POST: /Review/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Review review)
        {
            if (ModelState.IsValid)
            {
                db.Reviews.Add(review);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            ModelState.AddModelError("", "Invalid title or description");
            return View(review);
        }

        //
        // GET: /Review/Delete/5
        [Authorize(Roles = "Administrator")]
        public ActionResult Delete(int id = 0)
        {
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            ViewBag.LeaderBoard = Utilities.GetTopScores(10);
            return View(review);
        }

        //
        // POST: /Review/Delete/5
        [Authorize(Roles = "Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Review review = db.Reviews.Find(id);
            db.Reviews.Remove(review);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}