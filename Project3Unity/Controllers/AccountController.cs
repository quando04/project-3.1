﻿using Project3Unity.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Project3Unity.Controllers
{
    [Authorize(Roles = "Administrator,Users")]
    public class AccountController : Controller
    {
        private project3Entities db = new project3Entities();

        //
        // GET: /Account/Details/username

        public ActionResult Details(string username = null)
        {
            Account account = db.Accounts.First(model => model.accUsername.Equals(username));
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        //
        // GET: /Account/Create

        [AllowAnonymous]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Account/Create

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Account account, string confirmPassword)
        {
            if (ModelState.IsValid && account.accPassword.Equals(confirmPassword))
            {
                if (db.Accounts.Where(m => m.accEmail.Equals(account.accEmail)
                    || m.accUsername.Equals(account.accUsername)) != null)
                {
                    ModelState.AddModelError("", "Email or username entered is already taken");
                    return View(account);
                }
                db.Accounts.Add(account);

                var air = new AccountsInRole();
                air.accEmail = account.accEmail;
                air.roleID = 2;
                db.AccountsInRoles.Add(air);

                db.SaveChanges();
                return RedirectToAction("Login", "Account");
            }
            ModelState.AddModelError("", "Invalid email, username or password");
            return View(account);
        }

        //
        // GET: /Account/Edit/username

        public ActionResult Edit()
        {
            return View(new Account());
        }

        //
        // POST: /Account/Edit/username

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Exclude = "accEmail,accUsername")]Account account, string oldPassword, string confirmPassword)
        {
            Account old = db.Accounts.Find(User.Identity.Name);
            if (old == null)
            {
                return HttpNotFound();
            }
            if (!oldPassword.Equals(old.accPassword))
            {
                ModelState.AddModelError("", "Invalid old password");
                return View(new Account());
            }
            if (!account.accPassword.Equals(confirmPassword))
            {
                ModelState.AddModelError("", "Confirm password failed");
                return View(new Account());
            }

            old.accPassword = account.accPassword;
            var entry = db.Entry(old);
            entry.Property(e => e.accPassword).IsModified = true;
            db.SaveChanges();
            return RedirectToAction("Details", "Account", routeValues: new { username = old.accUsername });
        }

        // GET: /Account/Login

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return Logout();
            }
            return View();
        }

        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Account account)
        {
            Account a = db.Accounts.SingleOrDefault(
                    m => m.accEmail.Equals(account.accEmail)
                    && m.accPassword.Equals(account.accPassword));
            if (a != null)
            {
                if (Membership.ValidateUser(account.accEmail, account.accPassword))
                {
                    FormsAuthentication.RedirectFromLoginPage(a.accEmail, false);
                }
                ModelState.AddModelError("", "Incorrect email and/or password");
                return View("Login", account);
            }
            ModelState.AddModelError("", "The email and/or password provided is incorrect");
            return View("Login", account);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Remove("username");
            return RedirectToAction("Login", "Account");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}