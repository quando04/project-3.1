﻿function checkExtension(fileName, hashExt) {
    var ext = fileName.slice(fileName.lastIndexOf(".")).toLowerCase();
    if (hashExt[ext] || fileName === "") {
        return true;
    } else {
        return false;
    }
}

function isImage(fileExt) {
    var imageExts = [".jpg", ".jpeg", ".png", ".gif"];
    for (var i = 0; i < imageExts.length; i++) {
        if (fileExt === imageExts[i]) {
            return true;
        }
    }
    return false;
}

function isVideo(fileExt) {
    var videoExts = [".avi", ".mp4", ".mkv", ".wmv", ".mov"];
    for (var i = 0; i < videoExts.length; i++) {
        if (fileExt === videoExts[i]) {
            return true;
        }
    }
    return false;
}