﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project3Unity.Models
{
    public class ScoreRank
    {
        public int Rank { get; set; }
        public HighScore Score { get; set; }
    }
}