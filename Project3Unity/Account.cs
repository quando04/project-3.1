//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Project3Unity
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class Account
    {
        public Account()
        {
            this.Bugs = new HashSet<Bug>();
            this.Comments = new HashSet<Comment>();
            this.HighScores = new HashSet<HighScore>();
            this.Reviews = new HashSet<Review>();
            this.Suggestions = new HashSet<Suggestion>();
            this.AccountsInRoles = new HashSet<AccountsInRole>();
        }

        [DisplayName("Email")]
        [Required]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Invalid email!")]
        [DataType(DataType.EmailAddress)]
        [StringLength(50, ErrorMessage = "Email can have at most 50 characters!")]
        public string accEmail { get; set; }

        [DisplayName("Username")]
        [Required]
        [RegularExpression(@"^[a-z0-9_-]+$", ErrorMessage = "Invalid username!")]
        [DataType(DataType.Text)]
        [StringLength(15, MinimumLength = 6, ErrorMessage = "Username must be between 6-15 characters!")]
        public string accUsername { get; set; }

        [DisplayName("Password")]
        [Required]
        [RegularExpression(@"^[a-z0-9_-]+$", ErrorMessage = "Invalid password!")]
        [DataType(DataType.Password)]
        [StringLength(30, MinimumLength = 8, ErrorMessage = "Password must be between 8-30 characters!")]
        public string accPassword { get; set; }
    
        public virtual ICollection<Bug> Bugs { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<HighScore> HighScores { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Suggestion> Suggestions { get; set; }
        public virtual ICollection<AccountsInRole> AccountsInRoles { get; set; }
    }
}
