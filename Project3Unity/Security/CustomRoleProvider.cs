﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Project3Unity.Security
{
    public class CustomRoleProvider : RoleProvider
    {
        public override string ApplicationName { get; set; }

        public override string[] GetRolesForUser(string email)
        {
            using (project3Entities context = new project3Entities())
            {
                Account a = context.Accounts.FirstOrDefault(x => x.accEmail.Equals(email));
                if (a == null)
                {
                    throw new ProviderException("The email " + email + " does not exist!");
                }
                else
                {
                    var airs = a.AccountsInRoles;
                    if (airs != null && airs.Count > 0)
                    {
                        string[] roles = new string[airs.Count];
                        for (int i = 0; i < airs.Count; i++)
                        {
                            roles[i] = airs.ElementAt(i).Role.roleName;
                        }
                        return roles;
                    }
                    return null; // No roles associated with this account
                }
            }
        }

        public override bool IsUserInRole(string email, string roleName)
        {
            using (project3Entities context = new project3Entities())
            {
                if (!RoleExists(roleName))
                {
                    throw new ProviderException("The role " + roleName + " does not exist!");
                }
                var roles = GetRolesForUser(email);
                if (roles != null)
                {
                    return roles.Contains(roleName);
                }
                return false; // No roles associated with this account
            }
        }

        public override void AddUsersToRoles(string[] emails, string[] roles)
        {
            using (project3Entities db = new project3Entities())
            {
                foreach (string role in roles)
                {
                    foreach (string email in emails)
                    {
                        if (db.Accounts.Find(email) != null)
                        {
                            if (RoleExists(role))
                            {
                                int roleID = db.Roles.First(m => m.roleName.Equals(role)).roleID;
                                db.AccountsInRoles.Add(new AccountsInRole() { roleID = roleID, accEmail = email });
                            }
                            else
                            {
                                throw new Exception("Role name not found!");
                            }
                        }
                        else
                        {
                            throw new Exception("Email not found!");
                        }
                    }
                }
                db.SaveChanges();
            }
        }

        public override void CreateRole(string roleName)
        {
            using (project3Entities db = new project3Entities())
            {
                if (!RoleExists(roleName))
                {
                    db.Roles.Add(new Role() { roleName = roleName });
                    db.SaveChanges();
                }
                else
                {
                    throw new ProviderException("The role " + roleName + " already exists!");
                }
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            using (project3Entities db = new project3Entities())
            {
                if (RoleExists(roleName))
                {
                    var airs = db.AccountsInRoles.Where(m => m.Role.roleName.Equals(roleName));
                    Role removedRole = db.Roles.First(m => m.roleName.Equals(roleName));
                    if (airs != null && airs.Count() > 0) // Role is populated
                    {
                        if (throwOnPopulatedRole)
                        {
                            throw new ProviderException("The role " + roleName + " is not empty!");
                        }
                        else
                        {
                            foreach (AccountsInRole air in airs)
                            {
                                db.AccountsInRoles.Remove(air);
                            }
                        }
                    }
                    db.Roles.Remove(removedRole);
                    db.SaveChanges();
                    return true;
                }
                throw new ProviderException("The role " + roleName + " does not exist!");
            }
        }

        public override string[] FindUsersInRole(string roleName, string emailToMatch)
        {
            using (project3Entities db = new project3Entities())
            {
                if (!RoleExists(roleName))
                {
                    throw new ProviderException("The role " + roleName + " does not exist!");
                }
                var emails = from e in db.AccountsInRoles
                             where e.Role.roleName.Equals(roleName) && e.accEmail.Contains(emailToMatch)
                             select e.accEmail;
                if (emails == null || emails.Count() == 0)
                {
                    return null;
                }
                return emails.ToArray();
            }
        }

        public override string[] GetAllRoles()
        {
            using (project3Entities db = new project3Entities())
            {
                var roles = from r in db.Roles select r.roleName;
                if (roles != null && roles.Count() > 0)
                {
                    return roles.ToArray();
                }
                return null;
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            using (project3Entities db = new project3Entities())
            {
                if (RoleExists(roleName))
                {
                    var airs = db.AccountsInRoles.Where(m => m.Role.roleName.Equals(roleName));
                    if (airs == null || airs.Count() == 0)
                    {
                        return null;
                    }
                    var emails = (from r in airs select r.accEmail).ToArray();
                    return emails;
                }
                throw new ProviderException("The role " + roleName + " does not exist!");
            }
        }

        public override void RemoveUsersFromRoles(string[] emails, string[] roleNames)
        {
            using (project3Entities db = new project3Entities())
            {
                foreach (string role in roleNames)
                {
                    if (RoleExists(role))
                    {
                        foreach (string email in emails)
                        {
                            var air = db.AccountsInRoles.First(m => m.accEmail.Equals(email) && m.Role.roleName.Equals(role));
                            if (air != null)
                            {
                                db.AccountsInRoles.Remove(air);
                            }
                        }
                    }
                }
                db.SaveChanges();
            }
        }

        public override bool RoleExists(string roleName)
        {
            using (project3Entities db = new project3Entities())
            {
                return db.Roles.First(m => m.roleName.Equals(roleName)) != null;
            }
        }
    }
}