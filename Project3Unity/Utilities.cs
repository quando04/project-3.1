﻿using PagedList.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project3Unity
{
    public class Utilities
    {
        private static project3Entities db = new project3Entities();

        public static IEnumerable<HighScore> GetTopScores(int top)
        {
            IQueryable<HighScore> scores = (from s in db.HighScores
                                            orderby s.srcHS descending
                                            select s).Take(top);
            return scores.ToList();
        }
    }

    public class CustomPagedListRenderOption : PagedListRenderOptions
    {
        private static PagedListRenderOptions defaultPagingOpt = new PagedListRenderOptions
        {
            MaximumPageNumbersToDisplay = 10,
            DisplayLinkToFirstPage = PagedListDisplayMode.IfNeeded,
            DisplayLinkToLastPage = PagedListDisplayMode.IfNeeded,
            LinkToFirstPageFormat = "First",
            LinkToLastPageFormat = "Last",
            //LinkToPreviousPageFormat = "Previous",
            //LinkToNextPageFormat = "Next"
        };
        public static PagedListRenderOptions DefaultPaging
        {
            get
            {
                return defaultPagingOpt;
            }
        }
    }
}